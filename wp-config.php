<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'edupress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'mysql');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+DoS$VB4]WP4MT*>-7Q#pQ3(!PQkiwPJ:`Q=RP$+Ul_Z=!(T&m^cN;>7,5SI6atd');
define('SECURE_AUTH_KEY',  'pHT#(69k<UM09([f:z4`U$y5p2<gkLUC8C!Fsp{Ql)Q58C704_zp9}cAG7:k;+ (');
define('LOGGED_IN_KEY',    'R[Q3R-:D(~6Q>l}dPpr>,301 Ud@u)=Qn~v:C;!B;=cMy,qr#IEt%,9QxIak0xK3');
define('NONCE_KEY',        '#VaS5r[fm79]N.3v/iA2atJZXG.cXBZ6DIWrG%_{A4>/]P}$|MU9<S8J-Y8UsG-g');
define('AUTH_SALT',        'um D:Le=cbEL>$/?G&kEQtOJmX}09G^<ahNa.qNKnmwcr9]r=US`#&60Ua9{}1kN');
define('SECURE_AUTH_SALT', '&a.&ix,)@?w>v|A%TuR0qMtM>YG=YlxB7Sdbt[US~[8A3|B;WVjX1-w%%hv{e 6q');
define('LOGGED_IN_SALT',   'EKd3]pRRvOI02v]zcgZ|8oE0UY`{V:bXQ05Y*vF?j*#!<.L$,@)EmbztGz;%QE[4');
define('NONCE_SALT',       '0K6W!)JbFMCfLSEd408 Z:ZYpVt$P,H*W>:JrEM!Clu{pG82I6%-D6)c&)-Z1:gB');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
