<?php
/*
Plugin Name: pinisi
Plugin URI:  https://edubox.pinisi.io
Description: Basic WordPress Plugin Header Comment
Version:     20160911
Author:      WordPress.org
Author URI:  https://developer.wordpress.org/
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: wporg
Domain Path: /languages
*/

// function export_sql()
// {
//     // do something
//     echo "yuhuaaa";
// }
// add_action('init', 'export_sql');


function my_custom_url_handler() {
	if (strpos($_SERVER["REQUEST_URI"], '/edupress/export/') !== false) {

    // if($_SERVER["REQUEST_URI"] == '/edupress/custom_url/') {
     //  $return = 'INSERT INTO ' . $table . ' (`quiz_id`, `lesson_id`, `title`, `text`, `choices`, `key_answer`, `created_at`, `updated_at`, `teacher_id`, `created_by`, `updated_by`, `file`, `type`, `point`, `choices_files`, `id_lama`, `share_status`, `share_teacher`, `trash`, `sync_status`, `stats_status`) VALUES();';
	    
	    // $backup_name = "edit_nama_file_ini_sesuai_nama_ujian_tanpa_spasi.sql";
     //    header('Content-Type: application/octet-stream');
     //    header("Content-Transfer-Encoding: Binary");
     //    header("Content-disposition: attachment; filename=\"" . $backup_name . "\"");
     //    echo $return;


		$uri = $_SERVER["REQUEST_URI"];
		$id_post = explode("/", $uri);
		$my_postid = $id_post[3];
  	 	
		$content_post = get_post($my_postid);
		$received_str = $content_post->post_content;
		$judul = $content_post->post_title;
		// $content = apply_filters('the_content', $content);
		// $content = str_replace(']]>', ']]&gt;', $content);

			
		// $received_str = get_the_content();


		// $received_str = trim(preg_replace("/(\[.+?\])((?:[^\[])+)/", "$1" . PHP_EOL . "$2" . PHP_EOL, str_replace(PHP_EOL, "", $received_str)));
		// $received_str = preg_replace('/\s+/', '', $received_str);

		$dom = new DOMDocument();
		libxml_use_internal_errors(true);
		$dom->loadHTML($received_str);
		libxml_clear_errors();
		$dom->preserveWhiteSpace = false; // important!


	
	    $nodelist = $dom->getElementsByTagName('table');


		$the_result = array();
		$the_question = array();

		foreach ($nodelist as $tab) {			
			// if (($tab->nodeName == 'table')  && ($tab->getElementsByTagName('tr').length > 1) ){
			if ($tab->nodeName == 'table'){


			$liNumber = 0;
			$lewati = 0;	
			 foreach ( $tab->childNodes as $pp ) {
			 	 if($pp->hasChildNodes()) {
			 	 	foreach($pp->childNodes as $ppp) {
			            if ( $ppp->nodeName == 'tr' ) {
			               	$lewati++;
			            }
		        	 }   

		     	}
		    }



		    if ($lewati > 1) {
		        continue;
		    } 

		    // $the_result[] = $dom->saveHTML($tab);


			$soal ="";
			$jawaban ="";
			$jawabanA = "";
			$jawabanB = "";
			$jawabanC = "";
			$jawabanD = "";
			$jawabanE = "";
			$doAppenSoal = true;
			$doAppenJawabanA = true;
			$doAppenJawabanB = true;
			$doAppenJawabanC = true;
			$doAppenJawabanD = true;
			$doAppenJawabanE = true;
			
			// $paragraphs = str_replace("<table", "<p><table",  $paragraphs);	
			// $paragraphs = str_replace("</table>", "</table></p>",  $paragraphs);	
			foreach($tab->childNodes as $para)
			{
				if($para->hasChildNodes()) {
				foreach($para->childNodes as $paragraph1)
					{
				if($paragraph1->hasChildNodes()) {
				foreach($paragraph1->childNodes as $paragraph2)
						{

				if($paragraph2->hasChildNodes()) {
				foreach($paragraph2->childNodes as $paragraph)
						{
				if ($paragraph->nodeName == "ol" and $paragraph->hasChildNodes()) {
						foreach($paragraph->childNodes as $lis)
						{
								if($lis->hasChildNodes() and $lis->nodeName=='li') {
									$liNumber++;
								foreach($lis->childNodes as $li)
										{	
											switch ($liNumber) {
												case 1 :
													$jawabanA =$jawabanA . $dom->saveHTML($li);
													break;

												case 2 :
													$jawabanB =$jawabanB . $dom->saveHTML($li);
													break;
													

												case 3 :
													$jawabanC =$jawabanC . $dom->saveHTML($li);
													break;
													
												case 4 :
													$jawabanD =$jawabanD . $dom->saveHTML($li);
													break;
													

												case 5 :
													$jawabanE =$jawabanE . $dom->saveHTML($li);
													break;				
												
												default:
													# donotihng
													break;
											}
											
										}
								}		
						}		
				} else {

						if ($liNumber!=0) {
								            switch ($liNumber) {
												case 1 :
													$jawabanA =$jawabanA . $dom->saveHTML($paragraph);
													break;

												case 2 :
													$jawabanB =$jawabanB . $dom->saveHTML($paragraph);
													break;
													

												case 3 :
													$jawabanC =$jawabanC . $dom->saveHTML($paragraph);
													break;
													
												case 4 :
													$jawabanD =$jawabanD . $dom->saveHTML($paragraph);
													break;
													

												case 5 :
													$jawabanE =$jawabanE . $dom->saveHTML($paragraph);
													break;				
												
												default:
													# donotihng
													break;
											}
						}	else {
												
									if (strpos($dom->saveHTML($paragraph), 'KUNCI :') == true) {
									    // echo "-------SOALNYA------------";
									    $jawaban = $dom->saveHTML($paragraph);
									    $doAppenSoal = false;
									} else {
										if ($doAppenSoal) {
											$soal = $soal . $dom->saveHTML($paragraph);
										}
									}

									if (strpos($dom->saveHTML($paragraph), 'B.') == true ) {
									    // echo "-------SOALNYA------------";
									    $doAppenJawabanA = false;
									} else {
										if ($doAppenJawabanA && $doAppenSoal==false && strpos($dom->saveHTML($paragraph), 'KUNCI :') == false) {
											 $jawabanA = $jawabanA . $dom->saveHTML($paragraph);
										}
									}

									if (strpos($dom->saveHTML($paragraph), 'C.') == true ) {
									    // echo "-------SOALNYA------------";
									    $doAppenJawabanB = false;
									} else {
										if ($doAppenJawabanB && $doAppenSoal==false && $doAppenJawabanA==false && strpos($dom->saveHTML($paragraph), 'KUNCI :') == false) {
											$jawabanB = $jawabanB . $dom->saveHTML($paragraph);
										}
									}

									if (strpos($dom->saveHTML($paragraph), 'D.') == true ) {
									    // echo "-------SOALNYA------------";
									    $doAppenJawabanC = false;
									} else {
										if ($doAppenJawabanC && $doAppenSoal==false && $doAppenJawabanA==false && $doAppenJawabanB==false && strpos($dom->saveHTML($paragraph), 'KUNCI :') == false) {
											$jawabanC = $jawabanC . $dom->saveHTML($paragraph);
										}
									}

									if (strpos($dom->saveHTML($paragraph), 'E.') == true ) {
									    // echo "-------SOALNYA------------";
									    $doAppenJawabanD = false;
									} else {
										if ($doAppenJawabanD && $doAppenSoal==false && $doAppenJawabanA==false && $doAppenJawabanB==false && $doAppenJawabanC==false && strpos($dom->saveHTML($paragraph), 'KUNCI :') == false) {
											$jawabanD = $jawabanD . $dom->saveHTML($paragraph);
										}
									}

									if ($doAppenJawabanE && $doAppenSoal==false && $doAppenJawabanA==false && $doAppenJawabanB==false && $doAppenJawabanC==false && $doAppenJawabanD==false && strpos($dom->saveHTML($paragraph), 'KUNCI :') == false) {
											$jawabanE = $jawabanE . $dom->saveHTML($paragraph);

									}
								}

					}
					// echo $dom->saveHTML($paragraph)."+++++++";
					// echo $jawaban."</br>";

								}
							}
						}
					}
				}
				
			}

		}

			// echo "#########";
		
				 $jawaban = str_replace("<span lang=\"en-US\">", "",  $jawaban);
				 $jawaban = str_replace("</span>", "",  $jawaban);	
				 $jawaban = strip_tags($jawaban);
			     $jawaban_arr = explode(": ", $jawaban);
				 $jawaban = $jawaban_arr[1];
				 $jawaban = $jawaban[0];	
				 $result['jawaban'] = $jawaban;

				// $soal = utf8_decode($soal);
				 $result['soal'] = $soal;

				 $doc = new DOMDocument();
				 $doc->loadHTML($soal);
				 $xpath = new DOMXPath($doc);

				 $src = $xpath->evaluate("string(//img/@src)");

				 $path = $src;
				 $type = pathinfo($path, PATHINFO_EXTENSION);
				 $data = file_get_contents($path);
				 $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

				 $result['soal'] = str_replace($src,  $base64, $result['soal']);
					
				 $result['soal'] = str_replace("Â", "",  $result['soal']);
				 $result['soal'] = str_replace("<table", "<table border=\"1\"",  $result['soal']);
				 $result['soal'] = str_replace("<td", "<td align=\"center\"",  $result['soal']);




					

				 // $result['soal'] = substr($result['soal'], 2);

			 	 // $jawabanA = utf8_decode($jawabanA);	
				 $result['jawabanA'] = $jawabanA;


				 $doc = new DOMDocument();
				 $doc->loadHTML($jawabanA);
				 $xpath = new DOMXPath($doc);

				 $src = $xpath->evaluate("string(//img/@src)");

				 $path = $src;
				 $type = pathinfo($path, PATHINFO_EXTENSION);
				 $data = file_get_contents($path);
				 $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

				 $result['jawabanA'] = str_replace($src,  $base64, $result['jawabanA']);

				 $result['jawabanA'] = str_replace("A.", "",  $result['jawabanA']);
				 $result['jawabanA'] = str_replace("Â", "",  $result['jawabanA']);
				 $result['jawabanA'] = str_replace("<table", "<table border=\"1\"",  $result['jawabanA']);
				  $result['jawabanA'] = str_replace("<td", "<td align=\"center\"",  $result['jawabanA']);
				 

				 // $jawabanB = utf8_decode($jawabanB);	
				 $result['jawabanB'] = $jawabanB;

				  $doc = new DOMDocument();
				 $doc->loadHTML($jawabanB);
				 $xpath = new DOMXPath($doc);

				 $src = $xpath->evaluate("string(//img/@src)");

				 $path = $src;
				 $type = pathinfo($path, PATHINFO_EXTENSION);
				 $data = file_get_contents($path);
				 $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

				 $result['jawabanB'] = str_replace($src,  $base64, $result['jawabanB']);

				 $result['jawabanB'] = str_replace("B.", "",  $result['jawabanB']);
				 $result['jawabanB'] = str_replace("Â", "",  $result['jawabanB']);
				 $result['jawabanB'] = str_replace("<table", "<table border=\"1\"",  $result['jawabanB']);
				  $result['jawabanB'] = str_replace("<td", "<td align=\"center\"",  $result['jawabanB']);

				 // $jawabanC = utf8_decode($jawabanC);	
				 $result['jawabanC'] = $jawabanC;


				 $doc = new DOMDocument();
				 $doc->loadHTML($jawabanC);
				 $xpath = new DOMXPath($doc);

				 $src = $xpath->evaluate("string(//img/@src)");

				 $path = $src;
				 $type = pathinfo($path, PATHINFO_EXTENSION);
				 $data = file_get_contents($path);
				 $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

				 $result['jawabanC'] = str_replace($src,  $base64, $result['jawabanC']);

				 $result['jawabanC'] = str_replace("C.", "",  $result['jawabanC']);
				 $result['jawabanC'] = str_replace("Â", "",  $result['jawabanC']);
				 $result['jawabanC'] = str_replace("<table", "<table border=\"1\"",  $result['jawabanC']);
				  $result['jawabanC'] = str_replace("<td", "<td align=\"center\"",  $result['jawabanC']);

				 // $jawabanD = utf8_decode($jawabanD);	
				 $result['jawabanD'] = $jawabanD;


				  $doc = new DOMDocument();
				 $doc->loadHTML($jawabanD);
				 $xpath = new DOMXPath($doc);

				 $src = $xpath->evaluate("string(//img/@src)");

				 $path = $src;
				 $type = pathinfo($path, PATHINFO_EXTENSION);
				 $data = file_get_contents($path);
				 $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

				 $result['jawabanD'] = str_replace($src,  $base64, $result['jawabanD']);


				 $result['jawabanD'] = str_replace("D.", "",  $result['jawabanD']);
				 $result['jawabanD'] = str_replace("Â", "",  $result['jawabanD']);
				 $result['jawabanD'] = str_replace("<table", "<table border=\"1\"",  $result['jawabanD']);
				 $result['jawabanD'] = str_replace("<td", "<td align=\"center\"",  $result['jawabanD']);

				 // $jawabanE = utf8_decode($jawabanE);	
				 $result['jawabanE'] = $jawabanE;

				  $doc = new DOMDocument();
				 $doc->loadHTML($jawabanE);
				 $xpath = new DOMXPath($doc);

				 $src = $xpath->evaluate("string(//img/@src)");

				 $path = $src;
				 $type = pathinfo($path, PATHINFO_EXTENSION);
				 $data = file_get_contents($path);
				 $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

				 $result['jawabanE'] = str_replace($src,  $base64, $result['jawabanE']);

				 $result['jawabanE'] = str_replace("E.", "",  $result['jawabanE']);
				 $result['jawabanE'] = str_replace("Â", "",  $result['jawabanE']);
				 $result['jawabanE'] = str_replace("<table", "<table border=\"1\"",  $result['jawabanE']);
				  $result['jawabanE'] = str_replace("<td", "<td align=\"center\"",  $result['jawabanE']);

				 $the_result[] = $result;

		 	}
				  
		}
		// print_r($the_result);

		$result = array();
		$return = '';
		
		foreach ($the_result as $key => $value) {

			$result[$key]['text'] = $value['soal'];

			

			switch ($value['jawaban']) {
				case 'A':
					$result[$key]['key_answer'] = $value['jawabanA'];
					break;

				case 'B':
					$result[$key]['key_answer'] = $value['jawabanB'];
					break;

				case 'C':
					$result[$key]['key_answer'] = $value['jawabanC'];
					break;
					
				case 'D':
					$result[$key]['key_answer'] = $value['jawabanD'];
					break;
					
				case 'E':
					$result[$key]['key_answer'] = $value['jawabanE'];
					break;			
				
				default:
					$result[$key]['key_answer'] = $value['jawabanA'];
					break;
			}

			$result[$key]['choices'][]=$value['jawabanA'];
			$result[$key]['choices'][]=$value['jawabanB'];
			$result[$key]['choices'][]=$value['jawabanC'];
			$result[$key]['choices'][]=$value['jawabanD'];
			$result[$key]['choices'][]=$value['jawabanE'];

			$result[$key]['choices'] = json_encode($result[$key]['choices']);
			$result[$key]['choices'] = str_replace(",\"\"","",$result[$key]['choices']);
			
			$result[$key]['created_at'] = date("Y-m-d h:i:s");
			$result[$key]['updated_at'] = date("Y-m-d h:i:s");

			$result[$key]['teacher_id'] = "1";
			$result[$key]['created_by'] = "1";



			$result[$key]['text'] = addslashes($result[$key]['text']);
			$result[$key]['text'] = preg_replace("/\n/", "\\n", $result[$key]['text']);

			$result[$key]['choices'] = addslashes($result[$key]['choices']);
			$result[$key]['choices'] = preg_replace("/\n/", "\\n", $result[$key]['choices']);

			$result[$key]['key_answer'] = addslashes($result[$key]['key_answer']);
			$result[$key]['key_answer'] = preg_replace("/\n/", "\\n", $result[$key]['key_answer']);


			$return .= "\n";
			$return .= 'INSERT INTO questions (`quiz_id`, `lesson_id`, `title`, `text`, `choices`, `key_answer`, `created_at`, `updated_at`, `teacher_id`, `created_by`, `updated_by`, `file`, `type`, `point`, `choices_files`, `id_lama`, `share_status`, `share_teacher`, `trash`, `sync_status`, `stats_status`) VALUES(';
			$return .='NULL,NULL,"'.$judul.'","'.$result[$key]['text'].'","'.$result[$key]['choices'].'","'.$result[$key]['key_answer'].'","'.$result[$key]['created_at'].'","'.$result[$key]['updated_at'].'","'.$result[$key]['teacher_id'].'","'.$result[$key]['created_by'].'",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);';
			$return .= "\n\n\n";
			
		}

		
		$backup_name = preg_replace('/\s+/', '_', $judul);
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . $backup_name . ".sql\"");
        echo $return;

		
		// print_r($result);
      exit();
   }

   // echo $_SERVER["REQUEST_URI"];
}
add_action('init', 'my_custom_url_handler');


