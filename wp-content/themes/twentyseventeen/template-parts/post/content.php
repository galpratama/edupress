<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
	if ( is_sticky() && is_home() ) :
		echo twentyseventeen_get_svg( array( 'icon' => 'thumb-tack' ) );
	endif;
	?>
	<header class="entry-header">
		<?php
		if ( 'post' === get_post_type() ) {
			echo '<div class="entry-meta">';
				if ( is_single() ) {
					twentyseventeen_posted_on();
				} else {
					echo twentyseventeen_time_link();
					twentyseventeen_edit_link();
				};
			echo '</div><!-- .entry-meta -->';
		};

		if ( is_single() ) {
			the_title( '<h1 class="entry-title">', '</h1>' );
			
		} elseif ( is_front_page() && is_home() ) {
			the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
		} else {
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		}
		?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
		echo "<div class=\"ieu\">";
		$error = array();
		$received_str = get_the_content();


		// $received_str = trim(preg_replace("/(\[.+?\])((?:[^\[])+)/", "$1" . PHP_EOL . "$2" . PHP_EOL, str_replace(PHP_EOL, "", $received_str)));
		// $received_str = preg_replace('/\s+/', '', $received_str);

		$dom = new DOMDocument();
		libxml_use_internal_errors(true);
		$dom->loadHTML($received_str);
		libxml_clear_errors();
		$dom->preserveWhiteSpace = false; // important!
	
		$nodelist = $dom->getElementsByTagName('table');

		$the_result = array();
		$the_question = array();

		foreach ($nodelist as $tab) {			
			// if (($tab->nodeName == 'table')  && ($tab->getElementsByTagName('tr').length > 1) ){
			if ($tab->nodeName == 'table'){


			$liNumber = 0;
			$lewati = 0;	
			 foreach ( $tab->childNodes as $pp ) {
			 	 if($pp->hasChildNodes()) {
			 	 	foreach($pp->childNodes as $ppp) {
			            if ( $ppp->nodeName == 'tr' ) {
			               	$lewati++;
			            }
		        	 }   

		     	}
		    }



		    if ($lewati > 1) {
		        continue;
		    } 

		    // $the_result[] = $dom->saveHTML($tab);


			$soal ="";
			$jawaban ="";
			$jawabanA = "";
			$jawabanB = "";
			$jawabanC = "";
			$jawabanD = "";
			$jawabanE = "";
			$doAppenSoal = true;
			$doAppenJawabanA = true;
			$doAppenJawabanB = true;
			$doAppenJawabanC = true;
			$doAppenJawabanD = true;
			$doAppenJawabanE = true;
			
			// $paragraphs = str_replace("<table", "<p><table",  $paragraphs);	
			// $paragraphs = str_replace("</table>", "</table></p>",  $paragraphs);	
			foreach($tab->childNodes as $para)
			{
				if($para->hasChildNodes()) {
				foreach($para->childNodes as $paragraph1)
					{
				if($paragraph1->hasChildNodes()) {
				foreach($paragraph1->childNodes as $paragraph2)
						{

				if($paragraph2->hasChildNodes()) {
				foreach($paragraph2->childNodes as $paragraph)
						{
				if ($paragraph->nodeName == "ol" and $paragraph->hasChildNodes()) {
						foreach($paragraph->childNodes as $lis)
						{
								if($lis->hasChildNodes() and $lis->nodeName=='li') {
									$liNumber++;
								foreach($lis->childNodes as $li)
										{	
											switch ($liNumber) {
												case 1 :
													$jawabanA =$jawabanA . $dom->saveHTML($li);
													break;

												case 2 :
													$jawabanB =$jawabanB . $dom->saveHTML($li);
													break;
													

												case 3 :
													$jawabanC =$jawabanC . $dom->saveHTML($li);
													break;
													
												case 4 :
													$jawabanD =$jawabanD . $dom->saveHTML($li);
													break;
													

												case 5 :
													$jawabanE =$jawabanE . $dom->saveHTML($li);
													break;				
												
												default:
													# donotihng
													break;
											}
											
										}
								}		
						}		
				} else {

						if ($liNumber!=0) {
								            switch ($liNumber) {
												case 1 :
													$jawabanA =$jawabanA . $dom->saveHTML($paragraph);
													break;

												case 2 :
													$jawabanB =$jawabanB . $dom->saveHTML($paragraph);
													break;
													

												case 3 :
													$jawabanC =$jawabanC . $dom->saveHTML($paragraph);
													break;
													
												case 4 :
													$jawabanD =$jawabanD . $dom->saveHTML($paragraph);
													break;
													

												case 5 :
													$jawabanE =$jawabanE . $dom->saveHTML($paragraph);
													break;				
												
												default:
													# donotihng
													break;
											}
						}	else {
												
									if (strpos($dom->saveHTML($paragraph), 'KUNCI :') == true) {
									    // echo "-------SOALNYA------------";
									    $jawaban = $dom->saveHTML($paragraph);
									    $doAppenSoal = false;
									} else {
										if ($doAppenSoal) {
											$soal = $soal . $dom->saveHTML($paragraph);
										}
									}

									if (strpos($dom->saveHTML($paragraph), 'B.') == true ) {
									    // echo "-------SOALNYA------------";
									    $doAppenJawabanA = false;
									} else {
										if ($doAppenJawabanA && $doAppenSoal==false && strpos($dom->saveHTML($paragraph), 'KUNCI :') == false) {
											 $jawabanA = $jawabanA . $dom->saveHTML($paragraph);
										}
									}

									if (strpos($dom->saveHTML($paragraph), 'C.') == true ) {
									    // echo "-------SOALNYA------------";
									    $doAppenJawabanB = false;
									} else {
										if ($doAppenJawabanB && $doAppenSoal==false && $doAppenJawabanA==false && strpos($dom->saveHTML($paragraph), 'KUNCI :') == false) {
											$jawabanB = $jawabanB . $dom->saveHTML($paragraph);
										}
									}

									if (strpos($dom->saveHTML($paragraph), 'D.') == true ) {
									    // echo "-------SOALNYA------------";
									    $doAppenJawabanC = false;
									} else {
										if ($doAppenJawabanC && $doAppenSoal==false && $doAppenJawabanA==false && $doAppenJawabanB==false && strpos($dom->saveHTML($paragraph), 'KUNCI :') == false) {
											$jawabanC = $jawabanC . $dom->saveHTML($paragraph);
										}
									}

									if (strpos($dom->saveHTML($paragraph), 'E.') == true ) {
									    // echo "-------SOALNYA------------";
									    $doAppenJawabanD = false;
									} else {
										if ($doAppenJawabanD && $doAppenSoal==false && $doAppenJawabanA==false && $doAppenJawabanB==false && $doAppenJawabanC==false && strpos($dom->saveHTML($paragraph), 'KUNCI :') == false) {
											$jawabanD = $jawabanD . $dom->saveHTML($paragraph);
										}
									}

									if ($doAppenJawabanE && $doAppenSoal==false && $doAppenJawabanA==false && $doAppenJawabanB==false && $doAppenJawabanC==false && $doAppenJawabanD==false && strpos($dom->saveHTML($paragraph), 'KUNCI :') == false) {
											$jawabanE = $jawabanE . $dom->saveHTML($paragraph);

									}
								}

					}
					// echo $dom->saveHTML($paragraph)."+++++++";
					// echo $jawaban."</br>";

								}
							}
						}
					}
				}
				
			}

		}

			// echo "#########";
		
				 $jawaban = str_replace("<span lang=\"en-US\">", "",  $jawaban);
				 $jawaban = str_replace("</span>", "",  $jawaban);	
				 $jawaban = strip_tags($jawaban);
			     $jawaban_arr = explode(": ", $jawaban);
				 $jawaban = $jawaban_arr[1];
				 $jawaban = $jawaban[0];	
				 $result['jawaban'] = $jawaban;

				// $soal = utf8_decode($soal);
				 $result['soal'] = $soal;

				 $doc = new DOMDocument();
				 $doc->loadHTML($soal);
				 $xpath = new DOMXPath($doc);

				 $src = $xpath->evaluate("string(//img/@src)");

				 $path = $src;
				 $type = pathinfo($path, PATHINFO_EXTENSION);
				 $data = file_get_contents($path);
				 $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

				 $result['soal'] = str_replace($src,  $base64, $result['soal']);
					
				 $result['soal'] = str_replace("Â", "",  $result['soal']);
				 $result['soal'] = str_replace("<table", "<table border=\"1\"",  $result['soal']);
				 $result['soal'] = str_replace("<td", "<td align=\"center\"",  $result['soal']);




					

				 // $result['soal'] = substr($result['soal'], 2);

			 	 // $jawabanA = utf8_decode($jawabanA);	
				 $result['jawabanA'] = $jawabanA;


				 $doc = new DOMDocument();
				 $doc->loadHTML($jawabanA);
				 $xpath = new DOMXPath($doc);

				 $src = $xpath->evaluate("string(//img/@src)");

				 $path = $src;
				 $type = pathinfo($path, PATHINFO_EXTENSION);
				 $data = file_get_contents($path);
				 $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

				 $result['jawabanA'] = str_replace($src,  $base64, $result['jawabanA']);

				 $result['jawabanA'] = str_replace("A.", "",  $result['jawabanA']);
				 $result['jawabanA'] = str_replace("Â", "",  $result['jawabanA']);
				 $result['jawabanA'] = str_replace("<table", "<table border=\"1\"",  $result['jawabanA']);
				  $result['jawabanA'] = str_replace("<td", "<td align=\"center\"",  $result['jawabanA']);
				 

				 // $jawabanB = utf8_decode($jawabanB);	
				 $result['jawabanB'] = $jawabanB;

				  $doc = new DOMDocument();
				 $doc->loadHTML($jawabanB);
				 $xpath = new DOMXPath($doc);

				 $src = $xpath->evaluate("string(//img/@src)");

				 $path = $src;
				 $type = pathinfo($path, PATHINFO_EXTENSION);
				 $data = file_get_contents($path);
				 $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

				 $result['jawabanB'] = str_replace($src,  $base64, $result['jawabanB']);

				 $result['jawabanB'] = str_replace("B.", "",  $result['jawabanB']);
				 $result['jawabanB'] = str_replace("Â", "",  $result['jawabanB']);
				 $result['jawabanB'] = str_replace("<table", "<table border=\"1\"",  $result['jawabanB']);
				  $result['jawabanB'] = str_replace("<td", "<td align=\"center\"",  $result['jawabanB']);

				 // $jawabanC = utf8_decode($jawabanC);	
				 $result['jawabanC'] = $jawabanC;


				 $doc = new DOMDocument();
				 $doc->loadHTML($jawabanC);
				 $xpath = new DOMXPath($doc);

				 $src = $xpath->evaluate("string(//img/@src)");

				 $path = $src;
				 $type = pathinfo($path, PATHINFO_EXTENSION);
				 $data = file_get_contents($path);
				 $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

				 $result['jawabanC'] = str_replace($src,  $base64, $result['jawabanC']);

				 $result['jawabanC'] = str_replace("C.", "",  $result['jawabanC']);
				 $result['jawabanC'] = str_replace("Â", "",  $result['jawabanC']);
				 $result['jawabanC'] = str_replace("<table", "<table border=\"1\"",  $result['jawabanC']);
				  $result['jawabanC'] = str_replace("<td", "<td align=\"center\"",  $result['jawabanC']);

				 // $jawabanD = utf8_decode($jawabanD);	
				 $result['jawabanD'] = $jawabanD;


				  $doc = new DOMDocument();
				 $doc->loadHTML($jawabanD);
				 $xpath = new DOMXPath($doc);

				 $src = $xpath->evaluate("string(//img/@src)");

				 $path = $src;
				 $type = pathinfo($path, PATHINFO_EXTENSION);
				 $data = file_get_contents($path);
				 $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

				 $result['jawabanD'] = str_replace($src,  $base64, $result['jawabanD']);


				 $result['jawabanD'] = str_replace("D.", "",  $result['jawabanD']);
				 $result['jawabanD'] = str_replace("Â", "",  $result['jawabanD']);
				 $result['jawabanD'] = str_replace("<table", "<table border=\"1\"",  $result['jawabanD']);
				 $result['jawabanD'] = str_replace("<td", "<td align=\"center\"",  $result['jawabanD']);

				 // $jawabanE = utf8_decode($jawabanE);	
				 $result['jawabanE'] = $jawabanE;

				  $doc = new DOMDocument();
				 $doc->loadHTML($jawabanE);
				 $xpath = new DOMXPath($doc);

				 $src = $xpath->evaluate("string(//img/@src)");

				 $path = $src;
				 $type = pathinfo($path, PATHINFO_EXTENSION);
				 $data = file_get_contents($path);
				 $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

				 $result['jawabanE'] = str_replace($src,  $base64, $result['jawabanE']);

				 $result['jawabanE'] = str_replace("E.", "",  $result['jawabanE']);
				 $result['jawabanE'] = str_replace("Â", "",  $result['jawabanE']);
				 $result['jawabanE'] = str_replace("<table", "<table border=\"1\"",  $result['jawabanE']);
				  $result['jawabanE'] = str_replace("<td", "<td align=\"center\"",  $result['jawabanE']);

				 $the_result[] = $result;

		 	}
				  
		}
		//print_r($the_result);
		echo "</div>";	

		if (!empty($_GET['cetak'])) {
		//do something

		} 

		else 
		{
		?>
		<div class="table-responsive">
			  <table border="1" class="table table-hover">
			    <thead>
			      <tr>
			        <th>No.</th>
			        <th>Pertanyaan</th>
			      </tr>
			    </thead>
			    <tbody>
			      <?php
			        $no = 1;
			        foreach ($the_result as $key) { ?>
						<tr>
						  <td><?php echo $no; ?></td>
						  <td class="collapsible-row-container">
						    <div class="collapsible-row">
						      <div>
						        <?php 
						        if (empty($key["soal"])) {
						        	// echo "(tidak ada kunci)";
						        	$error[$no]['nosoal'] = true; 
						        } else {
						        	echo $key["soal"]; 	
						        }
						        
						        if (empty($key["jawaban"])) {
						        	// echo "(tidak ada kunci)";
						        	$error[$no]['kunci_jawaban'] = true; 
						        }
						        ?>
						      </div>
						      <br/>
						      <div class="clearfix"></div>

						     <div class="exam-answer">
						       
						        <?php
						        if($key["jawaban"]=="A"){
						        ?>
						        <div class="alert alert-success" role="alert">
						        <?php
						            }else{
						        ?>
						        <div class="alert alert-danger" role="alert">
						        <?php
						            }
						        ?>
						          <?php
						           

						            if (empty($key["jawabanA"])) {
						        	// echo "(tidak ada kunci)";
							        	$error[$no]['jawabanA'] = true; 
							        } else {
							        	 echo $key["jawabanA"];
							        }
						          ?>
						        </div>
						      </div>
						    </div>


						     <div class="exam-answer">
						       
						        <?php
						        if($key["jawaban"]=="B"){
						        ?>
						        <div class="alert alert-success" role="alert">
						        <?php
						            }else{
						        ?>
						        <div class="alert alert-danger" role="alert">
						        <?php
						            }
						        ?>
						          <?php
						              if (empty($key["jawabanB"])) {
						        	// echo "(tidak ada kunci)";
							        	$error[$no]['jawabanB'] = true; 
							        } else {
							        	 echo $key["jawabanB"];
							        }
						          ?>
						        </div>
						      </div>
						    </div>

						     <div class="exam-answer">
						       
						        <?php
						        if($key["jawaban"]=="C"){
						        ?>
						        <div class="alert alert-success" role="alert">
						        <?php
						            }else{
						        ?>
						        <div class="alert alert-danger" role="alert">
						        <?php
						            }
						        ?>
						          <?php
						             if (empty($key["jawabanC"])) {
						        	// echo "(tidak ada kunci)";
							        	$error[$no]['jawabanC'] = true; 
							        } else {
							        	 echo $key["jawabanC"];
							        }
						          ?>
						        </div>
						      </div>
						    </div>

						     <div class="exam-answer">
						       
						        <?php
						        if($key["jawaban"]=="D"){
						        ?>
						        <div class="alert alert-success" role="alert">
						        <?php
						            }else{
						        ?>
						        <div class="alert alert-danger" role="alert">
						        <?php
						            }
						        ?>
						          <?php
						              if (empty($key["jawabanD"])) {
						        	// echo "(tidak ada kunci)";
							        	$error[$no]['jawabanD'] = true; 
							        } else {
							        	 echo $key["jawabanD"];
							        }
						          ?>
						        </div>
						      </div>
						    </div>

						     <div class="exam-answer">
						       
						        <?php
						        if($key["jawaban"]=="E"){
						        ?>
						        <div class="alert alert-success" role="alert">
						        <?php
						            }else{
						        ?>
						        <div class="alert alert-danger" role="alert">
						        <?php
						            }
						        ?>
						          <?php
						              if (empty($key["jawabanE"])) {
						        	// echo "(tidak ada kunci)";
							        	$error[$no]['jawabanE'] = true; 
							        } else {
							        	 echo $key["jawabanE"];
							        }
						          ?>
						        </div>
						      </div>
						      <br/><br/>
						    </div>

						  </td>
						</tr>



			          <?php

			          $no++;
			        }
			      ?>
			    </tbody>
			  </table>
			</div>
			<?php } 
		
			if (empty($error)) {
				echo "<a href=".get_site_url()."/export/".get_the_id().">DOWNLOAD</a>";
			} else {
				// print_r($error);
				echo "Harap perbaiki dulu error berikut ini:"."</br>";
				foreach ($error as $key => $value) {
					echo "Soal No.".$key." tidak ada :"."</br>";
					foreach ($value as $key2 => $value2) {
						echo "- ".$key2."</br>";
					}
				}
			}
			?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
